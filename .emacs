(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 ;; cuaモードをONにする
 '(cua-mode t)
 ;; 行番号を表示する
 '(global-linum-mode t)
 ;; ツールバーを隠す
 '(tool-bar-mode nil)
 ;; 対応する括弧を目立たせる
 '(show-paren-mode t)
)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.

 ;; 背景を黒色にする
 '(default ((t
             (:background "black" :foreground "#FFFFFF")
             )))
 '(cursor ((((class color)
             (background dark))
            (:background "#FFFFFF"))
           (((class color)
             (background light))
            (:background "#999999"))
           (t ())
           ))
)
;; 背景を半透明にする(90%)
(add-to-list 'default-frame-alist '(alpha . (0.90 0.90)))
